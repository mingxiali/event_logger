CREATE TABLE events (
    time TIMESTAMP NOT NULL,
    event VARCHAR(10) NOT NULL,
    trigger VARCHAR(100) NOT NULL,
    freq VARCHAR(10) NOT NULL,
    alerted BOOLEAN DEFAULT NULL,
    error TEXT,
    client_name VARCHAR (80) NOT NULL,
    CONSTRAINT time_event_trigger_freq PRIMARY KEY(time,event, trigger, freq)
);


WITH new_values (time, event, trigger, freq, alerted, error, client_name) as (
values 
    {element}
),
upsert as
( 
update events m 
    set alerted = nv.alerted,
           error = nv.error,
           client_name = nv.client_name
FROM new_values nv
WHERE m.time = to_timestamp(nv.time, 'YYYY-MM-DD hh24:mi:ss')::timestamp and m.event = nv.event and m.trigger = nv.trigger and m.freq = nv.freq
RETURNING m.*
)
INSERT INTO events (time, event, trigger, freq, alerted, error, client_name)
SELECT to_timestamp(time, 'YYYY-MM-DD hh24:mi:ss'), event, trigger, freq, alerted, error, client_name 
FROM new_values
WHERE NOT EXISTS (SELECT 1 
                FROM upsert up 
                WHERE up.time = to_timestamp(new_values.time, 'YYYY-MM-DD hh24:mi:ss')::timestamp and up.event = new_values.event and up.trigger = new_values.trigger and up.freq = new_values.freq);


INSERT INTO events (time, event, trigger, freq, alerted, error, client_name)
                        VALUES {element}
                        ON CONFLICT ON CONSTRAINT  time_event_trigger_freq DO UPDATE
                            SET alerted = excluded.alerted,
                            error = excluded.error, client_name = excluded.client_name;