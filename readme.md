# Event Logger Project

## Introduction
This is Pythonic project that keeps reading the target log file and extract event, timestamp and error message from the log file and keep a record in csv/database tables.

## Quick start
### Export variable to direct postgres driver
```bash
export LD_LIBRARY_PATH=/opt/postgres/10-pgdg/lib
```
### Install dependencies - not used by the lite version
```bash
pipenv install
```
### Activate environment and run the script
```bash
pipenv shell
python event_logger.py
```
### For lite version which works under current environment
```bash
python event_logger_lite.py
```

## Run as a backend service
Setup a cron task for this script to run after booting

## Features
1. Read config from the configuration.xml
2. Monitor the mtime change of timestamp.log file and update the csv/table with the latest new entries
3. Capture the error message and store it in the csv/table
4. Identify failed task and alert the correspondent developer
5. Identify incomplete run before a given time if no running process detected and alert the person in charge

## Flowchart
![flowchart](./docs/event_logger.png)

