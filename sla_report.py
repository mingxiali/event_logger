from datetime import datetime, timedelta
import os

# This script runs on each monday 7:00 a.m. by cron

send_to = "Javed.Mushtaq@gbst.com"

today = datetime.today()
last_week_start = today + timedelta(days=-7)

today_str = today.strftime('%Y%m%d')
last_week_start_str = last_week_start.strftime('%Y%m%d')
cmd_template = """export PGPASSWORD=Aih1Ooquae2w; /opt/postgres/10-pgdg/bin/64/psql -h qds-taxanalyser-prod-db -U qdscommon -d common -c "\copy (Select * from events where time>='{start_date}' and time<='{end_date}' order by time, trigger, event) TO STDOUT WITH DELIMITER AS ',' NULL AS ''  CSV HEADER"  > /data/client/mingl/shell/db/sla_events.csv"""
cmd_str = cmd_template.format(start_date=today_str, end_date=last_week_start_str)

# {message} | mailx -a {attachment} -s {subject} {target_email}
mail_cmd_template = """mail.py -s "{mail_subject}" -t {send_to} {attachment}"""
mail_subject = "SLA events since {start_date}".format(start_date=last_week_start_str)
attachment = "/data/client/mingl/shell/db/sla_events.csv"
mail_cmd = mail_cmd_template.format(attachment=attachment, mail_subject=mail_subject, send_to=send_to)
# execute the  command to generate the csv file
os.system(cmd_str)
os.system(mail_cmd)

