import os
import csv
import numpy as np
from datetime import datetime

def time_formatter(t_format):
    if len(t_format) == 16:
        ret = datetime.strptime(t_format, '%d/%m/%Y %H:%M')
    else:
        ret = datetime.strptime(t_format, '%Y-%m-%d %H:%M:%S')
    return ret

with open(os.path.join('/data/client/log/morning_event_logger.csv'), 'rb') as f:
    reader = csv.reader(f)
    data = list(reader)
    header, data = data[0], data[1:]
# reformat
type_str = [object] * len(header)
dtype = list(zip(header, type_str))

rdata = []
for idx, row in enumerate(data):
    row.append(row[2].replace('START', '').replace('END', '').replace(' - ', ''))
    row.append(idx)
    row[0] = time_formatter(row[0])
    rdata.append(tuple(row))

# data = [tuple(x) for x in data]
dtype.append(('trigger_event', object))
dtype.append(('original_index', object))
df = np.array(rdata, dtype=dtype)
# sort by time to keep the latest
df_sorted = sorted(df, key=lambda tup: tup[0], reverse=True)

df_sorted = [tuple(x) for x in df_sorted]
df_sorted = np.array(df_sorted, dtype=dtype)
print df