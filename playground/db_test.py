
import pandas as pd
from pangres import upsert, DocsExampleTable
from sqlalchemy import create_engine, VARCHAR, TIMESTAMP

# create a SQLalchemy engine
engine = create_engine('postgresql://data_writer:data_writer@@localhost:5432/event_logger')

dtype = {'time':TIMESTAMP}

df = pd.read_csv('/data/client/log/morning_event_logger.csv')

# primary key
df = df.set_index(['time', 'event', 'trigger', 'freq'])

df_db = pd.read_sql('SELECT * FROM events limit 10', engine)

# df.to_sql('events', engine, if_exists='append', index=True, index_label=['time', 'event', 'trigger', 'freq'])

upsert(engine=engine,
       df=df,
       table_name='events',
       if_row_exists='update',
       dtype=dtype)