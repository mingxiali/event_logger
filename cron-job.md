### Set up cronjob for scheduled SLA report sending
login to qds-proc2 as qdsclient and set cronjob with crontab -e and append these taskes to the end
```bash
30 6 * * Mon python /data/client/mingl/projects/event_logger/sla_report.py
45 6 1 * * python /data/client/mingl/projects/event_logger/sla_report_monthly.py
```