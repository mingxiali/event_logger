from datetime import datetime, timedelta
import os
import csv
import re
import time
import logging
import numpy as np
import subprocess
import psycopg2

logging.basicConfig(format='%(asctime)s | %(levelname)s | %(pathname)s:%(lineno)d | %(message)s', filename='events.log', encoding='utf-8', level=logging.INFO)
logger = logging.getLogger('event_logger')


def get_db_dict():
    db_dict = dict()
    db_dict['dbname'] = 'common'
    db_dict['user'] = 'qdscommon'
    db_dict['password'] = 'Aih1Ooquae2w'
    db_dict['host'] = 'qds-taxanalyser-prod-db'
    db_dict['port'] = 5432
    if os.uname()[1] == 'SYDLT2401':
        db_dict['dbname'] = 'event_logger'
        db_dict['user'] = 'data_writer'
        db_dict['password'] = 'data_writer@'
        db_dict['host'] = '127.0.0.1'
        db_dict['port'] = 5432
    elif os.uname()[1] == 'qds-devel2':
        db_dict['password'] = 'pe9eefohBach'
        db_dict['host'] = 'qds-taxanalyser-dr-db'
        db_dict['port'] = 5433
    return db_dict

def freq_to_filename_dict():
    ff_dict = dict()
    ff_dict['morning'] = 'morning'
    ff_dict['afternoon'] = '4pm'
    ff_dict['monthly'] = 'monthly'
    return ff_dict

def time_formatter(t_format):
    if len(t_format) == 16:
        ret = datetime.strptime(t_format, '%d/%m/%Y %H:%M')
    else:
        ret = datetime.strptime(t_format, '%Y-%m-%d %H:%M:%S')
    return ret

def get_client_name(key):
    ret = {
        'cfs_custom_index': 'Custom indices for Colonial First State',
        'cfs_300i': '300I ex Property for Colonial First State',
        'gics_perpetual': 'GICS for perpetual for rothschild/perpetual investments',
        'perpetual_300gics': 'Perpetual - 300 GICS SECTOR Changes',
        'bt_daily_aftertax_files': 'BT daily after tax files',
        'jfcp_daily_aftertax_files': 'JFCP daily after tax files',
        'cbus_daily_aftertax_files': 'CBUS Orbis daily after tax files',
        'cfsdaily_gics': 'CFS accumulation',
        'mercury_daily_gics': 'Mercury GICS attribution files',
        'fs_do_daily_gics': 'FirstState GICS attribution files',
        'bt_send_alerts_sde': 'BT - 150 ex 50 index changes check',
        'bt_back_office_load': 'BT - Backoffice Data',
        'bt_back_office_load_no_prop_split': 'BT - Backoffice Data NO PROP SPLIT',
        'bt_prelim_index_data': 'BT - Preliminary Custom Index Data',
        'bt_prelim_index_data_no_prop_split': 'BT - Preliminary Custom Index Data NO PROP SPLIT',
        'cfs_data': 'CFS Market Data',
        'black_rock_index_value': 'Blackrock - index values',
        'wilson_daily_files': 'Wilson HTM daily files',
        'daily_warakirri_upload': 'Daily Warakirri Upload',
        'warakirri_index_data': 'After-tax ginraw data for Warakirri',
        'warakirri_corporate_actions': 'Corporate actions for Warakirri',
        'cc_data': 'Continuum Capital Market Data',
        'bnp_daily_franked_data': 'BNP Paribas Franked Index Data',
        'challenger_frank_percent': 'Challenger franking percentage data for I-200',
        'atb_check_ratio': 'After tax benchmark - Ratio file check',
        'atb_data_flag': 'After tax benchmark - data flag',
        'solaris_wealth_attribution': 'Solaris Wealth - Attribution',
        'defect_check': 'Inspect errors in the log',
        'long_reach_index_value': 'Longreach index value data for I-300',
        'cfs_daily_gics': 'CFS - 4pm Index Data',
        'cc_data_decommision': 'Continuum Capital Market Data (Preliminary)',
        'esuper_fund_eod_prices': 'ESuperFund EOD Prices',
        'rimes_frank_data': '300 franked index to RBC',
        'pp_monthly_index_return': 'RBC Monthly data',
        'bt_monthly_gics_changes': 'BT -- Monthly GICS Changes',
        'perennial_monthly': 'Perennial Index Values XKO,B-3CX50,XJO,XSO,XTO',
        'wilson_weights_returns': 'Wilson HTM monthly files',
        'unisuper_monthly': 'UniSuper monthly after tax files',
        'sun_super_aftertax_data': 'SunSuper monthly after tax files',
        'perpetual_aftertax_data': 'Perpetual monthly after tax files',
        'catholic_aftertax_data': 'Catholic monthly after tax files',
        'state_super_aftertax_data': 'State Super monthly after tax files',
        'cooper_aftertax_data': 'Cooper monthly after tax files',
        'bnp_monthly_div_yield': 'BNP Paribas Franked Index Data',
        'cbus_monthly_aftertax_files': 'CBUS monthly after tax files'
    }
    return ret.get(key, 'other')

def upsert_to_db(df, db_dict=None):
    if db_dict is None:
        db_dict = get_db_dict()
    # form a string
    sql_tmp = """WITH new_values (time, event, trigger, freq, alerted, error, client_name) as (
                            values 
                                (%s, %s, %s, %s, %s, %s, %s)
                            ),
                            upsert as
                            ( 
                            update events m 
                                set alerted = nv.alerted,
                                    error = nv.error,
                                    client_name = nv.client_name
                            FROM new_values nv
                            WHERE m.time = to_timestamp(nv.time, 'YYYY-MM-DD hh24:mi:ss')::timestamp and m.event = nv.event and m.trigger = nv.trigger and m.freq = nv.freq
                            RETURNING m.*
                            )
                            INSERT INTO events (time, event, trigger, freq, alerted, error, client_name)
                            SELECT to_timestamp(time, 'YYYY-MM-DD hh24:mi:ss'), event, trigger, freq, alerted, error, client_name 
                            FROM new_values
                            WHERE NOT EXISTS (SELECT 1 
                                            FROM upsert up 
                                            WHERE up.time = to_timestamp(new_values.time, 'YYYY-MM-DD hh24:mi:ss')::timestamp and up.event = new_values.event and up.trigger = new_values.trigger and up.freq = new_values.freq);
    """
    # commit command
    # execute sql command
    conn = None
    try:
        conn = psycopg2.connect(**db_dict)
        cur = conn.cursor()
        # execute 1st statement
        for row in df.copy():
        # for ele in elements:
            alerted = True if str(row['alerted']).lower() == 'true' else False
            cur.execute(sql_tmp, (row['time'], row['event'], row['trigger'], row['freq'], alerted, row['error'], row['client_name']))
        # commit the transaction
        conn.commit()
        # close the database communication
        cur.close()
    except psycopg2.DatabaseError as e:
        logger.error('Database error found')
        logger.error(str(e))
        # logger.error(stmt)
    finally:
        if conn is not None and conn.closed == 0:
            conn.close()


class EventLogger(object):

    def __init__(self, delay=5):
        config_path, sections = os.environ['CONFIGURATION'].split('.xml')
        config_path = config_path + '.xml'
        self.max_wait_hour = 18
        import xml.etree.ElementTree as ET
        tree = ET.parse(config_path)
        root = tree.getroot()
        config_ele = root.find('.' + sections.replace(':', '/'))
        self.log_dir = config_ele.find('client').attrib['log_dir']
        self.owner_mail = config_ele.find('client').attrib['alertmail']
        # self.owner_mail = 'qds@gbst.com'
        self.freq = 'morning'
        self.alert_mail = 'qds@gbst.com'
        if os.uname()[1] == 'SYDLT2401':
            self.alert_mail = 'ming.li@gbst.com'
        self.settings = dict()
        self.delay = delay
        self.columns = ['time', 'event', 'trigger', 'freq', 'alerted', 'error', 'client_name']
        self.log_path = dict()
        self.output_file = dict()
        self.last_analyze_time = dict()
        self.last_mtime = dict()
        self.wait_for_incomplete_process = dict()
        self.new_row_identified = dict()
        self.freq = None
        self.on_launch()
       
    
    def on_launch(self):
        # TODO: parameters for each freq
         # settings for each freq
        fn_dict = freq_to_filename_dict()
        for freq in ['morning', 'afternoon', 'monthly']:
            self.freq = freq
            log_path = self.log_dir + '/{freq}/timestamps.log'.format(freq=fn_dict[freq])
            self.log_path[freq] = log_path
            self.output_file[freq] = self.log_dir + '/{freq}_event_logger.csv'.format(freq=freq)
            self.last_analyze_time[freq] = datetime.now()
            if not os.path.exists(self.log_path[freq]):
                logger.warning('No timestamp file found, creating a new file')
                open(self.log_path[freq], 'a').close()
            self.last_mtime[freq] = time.ctime(os.path.getmtime(self.log_path[freq]))
            self.wait_for_incomplete_process[freq] = 5
            self.new_row_identified[freq] = False
            logger.info('Event logger initialized')
            if not os.path.exists(self.output_file[freq]):
                try:
                    os.makedirs(os.path.dirname(self.output_file[freq]))
                except OSError:
                    logger.info('Directory already exists')
                # write csv header
                with open(self.output_file[freq], mode='w') as csv_file:
                    csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    csv_writer.writerow(self.columns)
            # kick off the reading process
            self.read_log()
            self.analyze()

    
    def analyze(self):
        # match trigger and event
        # if still no matching and no script running, alert a failed process
        # extract error messge in message column
        modified = False
        # read csv file
        with open(os.path.join(self.output_file[self.freq]), 'rb') as f:
            reader = csv.reader(f)
            data = list(reader)
            header, data = data[0], data[1:]
        # reformat
        type_str = [object] * len(header)
        dtype = list(zip(header, type_str))

        rdata = []
        for idx, row in enumerate(data):
            row.append(row[2].replace('START', '').replace('END', '').replace(' - ', ''))
            row.append(idx)
            row[0] = time_formatter(row[0])
            rdata.append(tuple(row))

        # data = [tuple(x) for x in data]
        dtype.append(('trigger_event', object))
        dtype.append(('original_index', object))
        df = np.array(rdata, dtype=dtype)
        # sort by time to keep the latest
        df_sorted = sorted(df, key=lambda tup: tup[0], reverse=True)

        df_sorted = [tuple(x) for x in df_sorted]
        df_sorted = np.array(df_sorted, dtype=dtype)
        # remove duplicates to keep the first of each kind
        kept_data = []
        existing_tuple = set()
        # loop over to only keep the first occurrance
        for idx, x in np.ndenumerate(df_sorted):
            if (x['trigger_event'], x['freq']) not in existing_tuple:
                existing_tuple.add((x['trigger_event'], x['freq']))
                kept_data.append(tuple(x))
        df_last_events = np.array(kept_data, dtype=dtype)
                # pick single END event and find the error 
        df_failed = df_last_events[(df_last_events['event'] == 'START') & (df_last_events['alerted'] != 'True')]
        if len(df_failed) == 0:
            logger.info('No failed process found')
        else:
            for idx, row in np.ndenumerate(df_failed):
                err_msg = self.extract_error(row['trigger_event'], row['time'])
                if len(err_msg) == 0:
                    # if it has been more than 5 minutes and no relavant process running, give alert
                    if row['time'] + timedelta(minutes=self.wait_for_incomplete_process[self.freq]) < datetime.now():
                        # check process
                        output = subprocess.check_output(["ps", "aux"])
                        if row['trigger_event'] in output:
                            err_msg = 'incomplete task'
                        else:
                            continue
                df[int(row['original_index'])]['error'] = err_msg
                if str(row['alerted']) in ('nan', ''):
                    self.send_mail(row['trigger_event'], err_msg)
                    # modify column
                    df[int(row['original_index'])]['alerted'] = True
                    modified = True
        # extract lastest complete task and fetch err msg
        df_complete = df_last_events[(df_last_events['event'] == 'END') & (df_last_events['alerted'] != 'True')]
        # keep on the first trigger event and freq
        if len(df_complete):
            end_time = df_complete['time'].max()
            time_mask = df_complete['time'] > (end_time - timedelta(hours=self.max_wait_hour))
            df_complete = df_complete[time_mask]
            logger.info(str(len(df_complete)) + ' finished tasks found')
            for idx, row in  np.ndenumerate(df_complete):
                err_msg = self.extract_error(row['trigger_event'], row['time'])
                if len(err_msg) > 0:
                    logger.warning('error found for ' + row["trigger_event"] + ' with ' + err_msg)
                    df[int(row['original_index'])]['error'] = err_msg
                    modified = True
                    if str(row['alerted']) != 'True':
                        self.send_mail(row['trigger_event'], err_msg)
                        df[int(row['original_index'])]['alerted'] = True
        # TODO: find all completed task before 16:40 p.m., identify any incomplete task for this day
        unique_keys = ['time', 'event', 'trigger', 'freq']
        if modified or self.new_row_identified[self.freq]:
            client_name = [get_client_name(x) for x in df['trigger_event']]
            df['client_name'] = client_name
            df = df[self.columns]
            try:
                # write to the database
                logger.info('Writing to DB')
                self.new_row_identified[self.freq] = False
                # remove entries one week ago
                mask_time = df['time'] > (max(list(df['time'])) - timedelta(days=7))
                df = df[mask_time]
                logger.info('Updating the csv file')
                time_col = [x.strftime('%Y-%m-%d %H:%M:%S') for x in df['time']]
                df['time'] = time_col
                # write to csv file
                if len(df):
                    upsert_to_db(df)
                    with open(self.output_file[self.freq], 'w') as outfile:
                        csv_writer = csv.writer(outfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        csv_writer.writerow(self.columns)
                        for idx, row in  np.ndenumerate(df):
                            csv_writer.writerow(list(row))

            except ValueError as e:
                logger.error('Exception when writing data to db')
                raise e

        self.last_analyze_time[self.freq] = datetime.now()

    def extract_error(self, trigger_event, start_time):
        # build in rule 
        err_msg = ''
        f_path = '/tmp/errors.{freq}Jobs.'.format(freq=self.freq) + trigger_event + '.log'
        if os.path.exists(f_path):
            # if last modified time for error log is greater than the start time, it means that the err msg is up to date
            f_time = datetime.fromtimestamp(os.path.getmtime(f_path))
            if (f_time + timedelta(seconds=10)) >= start_time:
                with open(f_path, 'r') as f:
                    lines = f.readlines()
                    err_msg = '|'.join(lines)
                # remove log
            os.remove(f_path)
        return err_msg

    def send_mail(self, event_trigger, err_msg):
        # TODO: collect errors and send a summary email
        logger.info('Sending email to ' + self.alert_mail + ' with subject of ' + event_trigger)
        now = datetime.now().strftime('%Y%m%d %H:%M:%S')
        cmd = "echo '{err_msg}' | mailx -s '{freq}Jobs: potential errors from log directory for task {event_trigger} @ {now}' {owner_mail}".format(err_msg=err_msg, event_trigger=event_trigger, now=now, owner_mail=self.alert_mail, freq=self.freq)
        # os.system(cmd)

    def read_log(self):
        logger.info('Reading log file')
        # TODO: filter out the logs that has been loaded
        with open(self.log_path[self.freq], 'r') as infile:
            # TODO: detect time
            csv_lines = set()
            with open(self.output_file[self.freq], 'r') as csv_file:
                csv_reader = csv.reader(csv_file)
                for row in csv_reader:
                    csv_lines.add(tuple(row[:-3]))

            with open(self.output_file[self.freq], 'a') as outfile:
                csv_writer = csv.writer(outfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                lines = infile.readlines()
                pat = '(.*)(?<=[START|END])(.*)'
                for line in lines:
                    m = re.search(pat, line)
                    m = m.groups()
                    if len(m) != 2:
                        raise ValueError
                    time_str = m[1][1:].strip()
                    if 'START' in m[0]:
                        event = 'START'
                    elif 'END' in m[0]:
                        event = 'END'
                    else:
                        raise ValueError
                    trigger = m[0].strip()
                    if (time_str, event, trigger, self.freq) not in csv_lines:
                        logger.info('New event Found  for ' + trigger)
                        csv_writer.writerow([time_str, event, trigger, self.freq, '', '', ''])
                        self.new_row_identified[self.freq] = True
    

    def run(self):
            # run as a service in the backend
            freqs = ['morning', 'afternoon', 'monthly']
            idx = 0
            while True:
                # catch exception when this file gets removed temporaly 
                # loop over frequence and  change the variables
                freq = freqs[idx]
                self.freq = freq
                if os.path.exists(self.log_path[freq]):
                    mtime = time.ctime(os.path.getmtime(self.log_path[freq]))
                    if mtime != self.last_mtime[freq]:
                        self.last_mtime[freq] = mtime
                        self.read_log()
                        self.analyze()

                # check if hasn't run analyze for a while, run it as the incomplete detection skips the entry if the task was kicked off less than one hour ago
                if datetime.now() > self.last_analyze_time[freq] +  timedelta(minutes=self.wait_for_incomplete_process[freq]):
                    self.analyze()
                time.sleep(self.delay)
                idx = (idx + 1) % len(freqs)


if __name__=='__main__':
    EventLogger().run()
