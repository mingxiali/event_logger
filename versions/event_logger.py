from datetime import datetime
import os
import csv
import re
import time
# from loguru import logger
import logging
import pandas as pd
import psutil
# from pangres import upsert
from sqlalchemy import create_engine


logging.basicConfig(format='%(asctime)s | %(levelname)s | %(message)s', filename='events.log', encoding='utf-8', level=logging.INFO)
logger = logging.getLogger('event_logger')
# logger.add('events.log', retention='5 days', backtrace=True, diagnose=True)


def get_client_name(key):
    ret = {
        'cfs_custom_index': 'Custom indices for Colonial First State',
        'cfs_300i': '300I ex Property for Colonial First State',
        'gics_perpetual': 'GICS for perpetual for rothschild/perpetual investments',
        'perpetual_300gics': 'Perpetual - 300 GICS SECTOR Changes',
        'bt_daily_aftertax_files': 'BT daily after tax files',
        'jfcp_daily_aftertax_files': 'JFCP daily after tax files',
        'cbus_daily_aftertax_files': 'CBUS Orbis daily after tax files',
        'cfsdaily_gics': 'CFS accumulation',
        'mercury_daily_gics': 'Mercury GICS attribution files',
        'fs_do_daily_gics': 'FirstState GICS attribution files',
        'bt_send_alerts_sde': 'BT - 150 ex 50 index changes check',
        'bt_back_office_load': 'BT - Backoffice Data',
        'bt_back_office_load_no_prop_split': 'BT - Backoffice Data NO PROP SPLIT',
        'bt_prelim_index_data': 'BT - Preliminary Custom Index Data',
        'bt_prelim_index_data_no_prop_split': 'BT - Preliminary Custom Index Data NO PROP SPLIT',
        'cfs_data': 'CFS Market Data',
        'black_rock_index_value': 'Blackrock - index values',
        'wilson_daily_files': 'Wilson HTM daily files',
        'daily_warakirri_upload': 'Daily Warakirri Upload',
        'warakirri_index_data': 'After-tax ginraw data for Warakirri',
        'warakirri_corporate_actions': 'Corporate actions for Warakirri',
        'cc_data': 'Continuum Capital Market Data',
        'bnp_daily_franked_data': 'BNP Paribas Franked Index Data',
        'challenger_frank_percent': 'Challenger franking percentage data for I-200',
        'atb_check_ratio': 'After tax benchmark - Ratio file check',
        'atb_data_flag': 'After tax benchmark - data flag',
        'solaris_wealth_attribution': 'Solaris Wealth - Attribution',
        'defect_check': 'Inspect errors in the log'
    }
    return ret.get(key, 'other')


class EventLogger(object):

    def __init__(self, log_path='/data/client/log/morning/timestamps.log', delay=5):
        config_path, sections = os.environ['CONFIGURATION'].split('.xml')
        config_path = config_path + '.xml'
        self.max_wait_hour = 18
        self.freq = 'morning'
        self.engine = create_engine('postgresql://data_writer:data_writer@@localhost:5432/event_logger')
        import xml.etree.ElementTree as ET
        tree = ET.parse(config_path)
        root = tree.getroot()
        config_ele = root.find('.' + sections.replace(':', '/'))
        log_dir = config_ele.find('client').attrib['log_dir']
        # DEBUG: USE ming.li@gbst.com
        # self.owner_mail = config_ele.find('client').attrib['alertmail']
        self.owner_mail = 'ming.li@gbst.com'
        log_path = log_dir + '/morning/timestamps.log'
        self.log_path = log_path
        self.delay = delay
        self.output_file = log_dir + '/morning_event_logger.csv'
        self.last_analyze_time = pd.Timestamp.now()
        if not os.path.exists(self.log_path):
            logger.warning('No timestamp file found, creating a new file')
            open(self.log_path, 'a').close()
        self.last_mtime = time.ctime(os.path.getmtime(self.log_path))
        self.wait_for_incomplete_process = 5
        self.new_row_identified = False
        logger.info('Event logger initialized')
        if not os.path.exists(self.output_file):
            # path
            try:
                os.makedirs(os.path.dirname(self.output_file))
            except OSError:
                logger.info('Directory already exists')
                # raise OSError('Folder already exists')
            # write csv header
            with open(self.output_file, mode='w') as csv_file:
                csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                csv_writer.writerow(['time', 'event', 'trigger', 'freq', 'alerted', 'error', 'client_name'])
        # kick off the reading process
        self.read_log()
        self.analyze()

    
    def analyze(self):
        # match trigger and event
        # if still no matching and no script running, alert a failed process
        # extract error messge in message column
        modified = False
        df = pd.read_csv(os.path.join(self.output_file))
        df.loc[:, 'time'] = df['time'].apply(pd.to_datetime)
        trigger_event = df['trigger'].str.replace('START', '').str.replace('END', '').str.replace(' - ', '')
        df.insert(0, 'trigger_event', trigger_event)
        df_entries = df.drop_duplicates(subset=['trigger_event', 'freq'], keep='last')
        # pick single END event and find the error 
        mask_start = df_entries['event'] == 'START'
        mask_no_alert = pd.isnull(df_entries['alerted'])
        df_failed = df_entries[mask_start & mask_no_alert]
        if len(df_failed) == 0:
            logger.info('No failed process found')
        else:
            for idx, row in df_failed.iterrows():
                err_msg = self.extract_error(row['trigger_event'], row['time'])
                if len(err_msg) == 0:
                    # if it has been more than 5 minutes and no relavant process running,, give alert
                    # check process
                    if (((row['time'] + pd.Timedelta(minutes=self.wait_for_incomplete_process)) < pd.Timestamp.now())
                     and (row['trigger_event'] not in (p.name() for p in psutil.process_iter()))):
                        # TODO: attach the log file if exists
                        err_msg = 'incomplete task'
                    else:
                        continue
                df.loc[idx, 'error'] = err_msg
                if str(row['alerted']) == 'nan':
                    self.send_mail(row['trigger_event'], err_msg)
                    # modify column
                    df.loc[idx, 'alerted'] = True
                    modified = True
        # extract lastest complete task and fetch err msg
        df_complete = df[(df['event'] == 'END') & (pd.isnull(df['alerted']))]
        df_complete = df_complete.drop_duplicates(subset=['trigger_event', 'freq'])
        # focus on the last 18 hours
        if len(df_complete):
            end_time = df_complete.time.max()
            time_mask = df_complete['time'] > (end_time - pd.Timedelta(hours=self.max_wait_hour))
            df_complete = df_complete[time_mask]
            logger.info(str(len(df_complete)) + ' finished tasks found')
            for idx, row in df_complete.iterrows():
                # print(row['trigger_event'])
                err_msg = self.extract_error(row['trigger_event'], row['time'])
                if len(err_msg) > 0:
                    logger.warning('error found for ' + row["trigger_event"] + ' with ' + err_msg)
                    df.loc[idx, 'error'] = err_msg
                    modified = True
                    if str(row['alerted']) == 'nan':
                        self.send_mail(row['trigger_event'], err_msg)
                        df.loc[idx, 'alerted'] = True
        # TODO: find all completed task before 16:40 p.m., identify any incomplete task for this day
        unique_keys = ['time', 'event', 'trigger', 'freq']
        if modified or self.new_row_identified:
            df.loc[:, 'client_name'] = df['trigger_event'].apply(get_client_name)
            df = df.drop(columns=['trigger_event'])
            df = df.drop_duplicates(subset=unique_keys)
            df = df.set_index(unique_keys)
            try:
                # upsert(engine=self.engine,
                #     df=df,
                #     table_name='events',
                #     if_row_exists='update')
                logger.info('Writing to DB')
                self.new_row_identified = False
                # remove entries one week ago
                df = df.reset_index()
                mask_time = df['time'] > (df['time'].max() + pd.Timedelta(days=-7))
                df = df[mask_time]
                logger.info('Updating the csv file')
                df.to_csv(os.path.join(self.output_file), index=False)

            except Exception as e:
                logger.error('Exception when writing data to db')
                raise e

        self.last_analyze_time = pd.Timestamp.now()

    def extract_error(self, trigger_event, start_time):
        # build in rule 
        err_msg = ''
        f_path = '/tmp/errors.morningJobs.' + trigger_event + '.log'
        if os.path.exists(f_path):
            # if last modified time for error log is greater than the start time, it means that the err msg is up to date
            if (pd.to_datetime(time.ctime(os.path.getmtime(f_path))) + pd.Timedelta(seconds=10)) >= start_time:
                with open(f_path, 'r') as f:
                    lines = f.readlines()
                    err_msg = '|'.join(lines)
                # remove log
            os.remove(f_path)
        return err_msg

    def send_mail(self, event_trigger, err_msg):
        logger.info('Sending email to ' + self.owner_mail + ' with subject of ' + event_trigger)
        now = pd.Timestamp.now().strftime('%Y%m%d %H:%M:%S')
        # cmd = f"echo {err_msg} | mailx -s 'morningJobs: potential errors from log directory for task {event_trigger} @ {now}' {self.owner_mail}"
        # TODO: uncomment the cmd
        cmd = 'echo sending email in debug mode'
        os.system(cmd)

    def read_log(self):
        logger.info('Reading log file')
        # TODO: filter out the logs that has been loaded
        with open(self.log_path, 'r') as infile:
            # TODO: detect time
            csv_lines = set()
            with open(self.output_file, 'r') as csv_file:
                csv_reader = csv.reader(csv_file)
                for row in csv_reader:
                    csv_lines.add(tuple(row[:-3]))

            with open(self.output_file, 'a') as outfile:
                csv_writer = csv.writer(outfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                lines = infile.readlines()
                pat = '(.*)(?<=[START|END])(.*)'
                for line in lines:
                    # get datetime
                    # today = datetime.now().strftime('%Y-%m-%d')
                    # write to csv file
                    # print line
                    m = re.search(pat, line)
                    m = m.groups()
                    if len(m) != 2:
                        raise ValueError
                    time_str = m[1][1:].strip()
                    if 'START' in m[0]:
                        event = 'START'
                    elif 'END' in m[0]:
                        event = 'END'
                    else:
                        raise ValueError
                    trigger = m[0].strip()
                    if (time_str, event, trigger, self.freq) not in csv_lines:
                        logger.info('New event Found  for ' + trigger)
                        csv_writer.writerow([time_str, event, trigger, self.freq])
                        self.new_row_identified = True
    

    # @logger.catch
    def run(self):
            # run as a service in the backend
            while True:
                # catch exception when this file gets removed temporaly 
                if os.path.exists(self.log_path):
                    mtime = time.ctime(os.path.getmtime(self.log_path))
                    if mtime != self.last_mtime:
                        self.last_mtime = mtime
                        self.read_log()
                        self.analyze()

                # check if hasn't run analyze for a while, run it as the incomplete detection skips the entry if the task was kicked off less than one hour ago
                if pd.Timestamp.now() > self.last_analyze_time + pd.Timedelta(minutes=self.wait_for_incomplete_process):
                    self.analyze()
                time.sleep(self.delay)


if __name__=='__main__':
    EventLogger().run()
