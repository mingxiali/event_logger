import psycopg2


def get_db_dict():
    db_dict = dict()
    db_dict['dbname'] = 'common'
    db_dict['user'] = 'qdscommon'
    db_dict['password'] = 'pe9eefohBach'
    db_dict['host'] = 'qds-taxanalyser-dr-db'
    db_dict['port'] = 5433
    # psycopg2.connect(**db_dict)
    return db_dict

stmt = """WITH new_values (time, event, trigger, freq, alerted, error, client_name) as (
                            values
                                ('2021-03-26 10:31:09','END','cc_data - END','morning',true,'','Continuum Capital Market Data')
                            ),
                            upsert as
                            (
                            update events m
                                set alerted = nv.alerted,
                                    error = nv.error,
                                    client_name = nv.client_name
                            FROM new_values nv
                            WHERE m.time = to_timestamp(nv.time, 'YYYY-MM-DD hh24:mi:ss')::timestamp and m.event = nv.event and m.trigger = nv.trigger and m.freq = nv.freq
                            RETURNING m.*
                            )
                            INSERT INTO events (time, event, trigger, freq, alerted, error, client_name)
                            SELECT to_timestamp(time, 'YYYY-MM-DD hh24:mi:ss'), event, trigger, freq, alerted, error, client_name
                            FROM new_values
                            WHERE NOT EXISTS (SELECT 1
                                            FROM upsert up
                                            WHERE up.time = to_timestamp(new_values.time, 'YYYY-MM-DD hh24:mi:ss')::timestamp and up.event = new_values.event and up.trigger = new_values.trigger and up.freq = new_values.freq);
"""
element = "('2021-03-26 11:11:11', 'END', 'test_event', 'daily', null, null, 'mingl')"
db_dict = get_db_dict()

try:
    conn = psycopg2.connect(**db_dict)
    cur = conn.cursor()
    # execute 1st statement
    # stmt = sql_tmp.format(element=element)
    # print stmt
    cur.execute(stmt)
    # commit the transaction
    conn.commit()
    # close the database communication
    cur.close()
except psycopg2.DatabaseError as e:
    print e.message
    # logger.error('Database error found')
